#
# Copyright (C) 2017 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Inherit from the common Open Source product configuration
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base_telephony.mk)

# Device identifier. This must come after all inclusions
PRODUCT_DEVICE := mix
PRODUCT_NAME := full_mix
PRODUCT_BRAND := DOOGEE
PRODUCT_MODEL := MIX
PRODUCT_MANUFACTURER := DOOGEE

# Inherit from jasmine device
$(call inherit-product, device/doogee/mix/device.mk)
$(call inherit-product, vendor/doogee/mix/mix-vendor.mk)
